package life;
import java.util.Scanner;



public class Main {
	
	public static void main(String[] args) {
		Life jogoDaVida = new Life();
		int loops;
		Scanner scan = new Scanner(System.in);
		System.out.println("Digite o número de simulações desejado: ");
		loops = scan.nextInt();
		jogoDaVida.inicializa();
		jogoDaVida.simulaVida(loops);
		
		
	}

}
