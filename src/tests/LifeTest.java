package tests;

import static org.junit.Assert.*;
import life.Life;

import org.junit.Before;
import org.junit.Test;

public class LifeTest {
	Life life;
	@Before
	public void setUp() throws Exception {
		 life = new Life();
		
	}

	@Test
	public void testInicializa() {
		life.inicializa();
		assert(life.matriz[0][0] == 1 || life.matriz[0][0] == 0);
	}
	@Test
	public void testInteracao() {
		life.matriz = new int[life.MAX][life.MAX];
		life.matriz[0][0] = 1;
		life.matriz[0][1] = 1;
		int[][] mat = new int[life.MAX][life.MAX];
		mat[0][0] = 0;
		assertArrayEquals(mat, life.iteração());
	}
	@Test
	public void testVizinhos() {
		life.matriz = new int[life.MAX][life.MAX];
		life.matriz[1][0] = 1;
		life.matriz[1][1] = 1;
		life.matriz[1][2] = 1;
		assertEquals(2, life.vizinhos(1, 1));
	}
	
	

}
